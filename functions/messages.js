const admin = require('firebase-admin');
const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors')({ origin: true });
const chunk = require('lodash.chunk');

const { messageTokenKey } = require('./config');
const { db, getTokens, setTokens } = require('./db');

/**
 * Filter current list of device IDs and remove any IDs that have become stale or unregistered with Firebase
 * @param {String} user - user identifier in Cloud Firestore
 * @param {String[]} tokens - current array of tokens for the given user
 * @param {Object[]} errors - array of messaging() response errors = { index, code }
 * @returns {undefined}
 */
const removeUnregisteredTokens = (user, tokens, errors = []) => {
    if (errors && errors.length > 0) {
        const unregisteredTokens = new Set();
        errors.forEach(err => {
            if (err.error.code === 'messaging/registration-token-not-registered') {
                unregisteredTokens.add(tokens[err.index]);
            }
        });
        if (unregisteredTokens.size > 0) {
            console.log(`Cleaning up ${unregisteredTokens.size} unregsitered tokens.`)
            const newTokens = tokens.filter(token => !unregisteredTokens.has(token));
            setTokens(user, newTokens);
        }
    }
}

/**
 * Middleware to validate requests against a known required token.  Any client trying to use
 * the /messages API must provide this bearer token in the header of each request.
 */
const validateMessageToken = async (req, res, next) => {
    try {
        if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))) {
            console.error('No Auth Bearer Token was provided with the request.');
            return res.status(403).send('Unauthorized');
        }

        let idToken;
        if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
            // Read the ID Token from the Authorization header.
            idToken = req.headers.authorization.split('Bearer ')[1];

            // Check the provided ID Token against expected token
            if (idToken !== messageTokenKey) {
                console.warn('Invalid token');
                return res.status(403).send('Unauthorized');
            }
        } else {
            // No token
            console.warn('No token provided');
            return res.status(403).send('Unauthorized');
        }

        // Everything looks good, so proceed with request
        return next();
    } catch (err) {
        console.error(err);
        return res.status(500).send(err);
    }
};

// Create Express App for messaging
const app = express();
// Use CORs
app.use(cors);
// Authenticate requests
// app.use(validateMessageToken);

/**
 * Define the /subscribe endpoint, which takes a user identifier and a topic and subscribes all
 * known devices for the given user to the provided topic.
 * 
 * POST /subscribe
 * {
 *    "user": "user_id",
 *    "topic": "topic_name"
 * }
 * 
 * Note: This method also removes any unregistered device IDs that are lingering in the database, since
 * the Firebase Cloud Messaging API reponse indicates which, if any, IDs are invalid.
 */
app.post('/subscribe', async (req, res) => {
    try {
        const { user, topic } = req.body;
        console.log(`Subscribe user: ${user} to topic: ${topic}`);

        const ids = await getTokens(user);
        if (ids.length === 0) {
            console.warn('No devices registered');
            return res.send();
        }

        console.log(`Subscribing for ${ids.length} devices`);
        const response = await admin.messaging().subscribeToTopic(ids, topic);

        if (response.failureCount > 0) {
            removeUnregisteredTokens(user, ids, response.errors);
        }
        return res.send(response);
    } catch (err) {
        console.error(err);
        return res.status(500).send(err);
    }
});

/**
 * Define the /unsubscribe endpoint, which takes a user identifier and a topic and unsubscribes all
 * known devices for the given user from the provided topic.
 * 
 * POST /unsubscribe
 * {
 *    "user": "user_id",
 *    "topic": "topic_name"
 * }
 *
 * Note: This method also removes any unregistered device IDs that are lingering in the database, since
 * the Firebase Cloud Messaging API reponse indicates which, if any, IDs are invalid.
 */
app.post('/unsubscribe', async (req, res) => {
    try {
        const { user, topic } = req.body;
        console.log(`Unsubscribe user: ${user} from topic: ${topic}`);

        const ids = await getTokens(user);
        if (ids.length === 0) {
            console.warn('No devices registered');
            return res.send();
        }

        console.log(`Unsubscribing for ${ids.length} devices`);
        const response = await admin.messaging().unsubscribeFromTopic(ids, topic);

        if (response.failureCount > 0) {
            removeUnregisteredTokens(user, ids, response.errors);
        }

        return res.send(response);
    } catch (err) {
        console.error(err);
        return res.status(500).send(err);
    }
});

/**
 * Define the /send endpoint, which publishes a message to the Firebase Cloud Messaging service.
 * 
 * This method simply passes the POST body as the message data for Firebase, so either 'data' or 'notification'
 * messages are supported.
 * 
 * POST /unsubscribe
 * 
 * Data Example:
 * {
 *    "topic": "topic_name",
 *    "data": {
 *        "foo": 1,
 *        "bar": ['a', 'b', 'c'],
 *        "baz": "Hey! You got some data!",
 *    }
 * }
 * 
 * Notification Example:
 * {
 *    "topic": "topic_name",
 *    "notification": {
 *        "title": "Hey you!",
 *        "body": "Read some stuff, and expand me to see more"
 *    }
 * }
 */
app.post('/send', async (req, res) => {
    try {
        const { users = [], topic, notification, data } = req.body;
        // Send a message to devices subscribed to a topic, if provided.
        if (topic) {
            console.log(`Posting message for topic: ${req.body.topic}`);
            const response = await admin.messaging().send(req.body);
    
            return res.send(response);
        }

        // Send message explicitly to list of devices if users are provided with no topic
        if (users.length > 0) {
            // Build array of doc refs for all users
            const refs = [];
            for (let i = 0; i < users.length; ++i) {
                refs.push(
                    db.collection('device_ids').doc(users[i])
                );
            }
            // Get all user docs
            const userDocs = await db.getAll(...refs, { fieldMask: ['ids'] });
            // If any user docs were returned, consolidate all device IDs
            if (userDocs.length > 0) {
                // Flatten all ID arrays into a single array
                const deviceIds = userDocs.flatMap(doc => {
                    if (!doc.exists) {
                        return [];
                    }
                    const { ids = [] } = doc.data();
                    return ids;
                });
                // push the messages in groups of 500 (max for sendMulticast)
                chunk(deviceIds, 500).forEach(deviceChunk => {
                    admin.messaging().sendMulticast({
                        tokens: deviceChunk,
                        notification,
                        data,
                    });
                });
                return res.send(`Message sent to ${userDocs.length} users across ${deviceIds.length} devices.`);
            }
        }

        return res.send('No message sent. No user devices found');
    } catch (err) {
        console.error(err);
        return res.status(500).send(err);
    }
});


module.exports = functions.https.onRequest(app);