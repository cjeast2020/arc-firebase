/*
 * serviceAccountKey.json can be downloaded from your Firebase project console
 * at Project Settings -> Service Accounts
 * 
 * 1) Generate new private key
 * 2) Save as 'serviceAccountKey.json' in 'secret/' directory
 * 
 * Firebase project.
 */
// const serviceAccount = require('./secret/serviceAccountKey.json');
const serviceAccount = {};

/*
 * The Firebase databaseURL can also be found under Project Settings -> Service Accounts
 * in the Firebase console, along with the serviceAccount keys.
 * 
 * Example:
 * const databaseURL = "https://<project_id>.firebaseio.com"
 */
const databaseURL = '';

/*
 * The messageToken.json file contains a secret token used to authenticate any client
 * wishing to use the messages API.  This should be generated and saved for your application
 * and then be added as a header Bearer Token for any /messages requests.
 * 
 * Expected format:
 * {
 *    "token": "the_secret_token_here"
 * }
 */
// const { token: messageTokenKey } = require('./secret/messageToken.json');
const messageTokenKey = '';

// Export the configuration settings
module.exports = {
    serviceAccount,
    databaseURL,
    messageTokenKey,
};