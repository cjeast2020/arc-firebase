const admin = require('firebase-admin');
const { serviceAccount, databaseURL } = require('./config');


// Initial the Admin SDK with Firebase project settings
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL,
});
// Create a Firestore DB instance
const db = admin.firestore();

// Export the DB instance
exports.db = db;

/**
 * Fetch all of the device ID tokens for a given user
 * @param {string} user - user identifier in Cloud Firestore
 * @returns {Promise<String[]>} - promise yielding an array of device IDs for the user
 */
exports.getTokens = async user => {
    const doc = await db.collection('device_ids').doc(user).get();
    if (!doc.exists) {
        console.warn('No user document found, no devices registered');
        return [];
    }

    const { ids } = doc.data();
    return ids;
};

/**
 * Replace the array of device IDs for a given user
 * @param {String} user - user identifier in Cloud Firestore
 * @param {String[]} ids - array of device IDs to store for the user (replaces existing IDs)
 * @returns {Promise}
 */
exports.setTokens = (user, ids) => db.collection('device_ids').doc(user).update({ ids });