# Firebase Cloud Messaging API #

This repository contains code for the proposed solution for DigitalARC and Firebase Cloud Messaging integration.

## What is this repository for? ##

This repository provides integration with Firebase Cloud Messaging for environments that do not support the Firebase SDK by exposing `subscribe`, `unsubscribe`, and `send` methods via a REST API through Firebase Cloud Functions.  

_Note: Supports FCM by topic only, currently.  Does not support messaging for explicit devices, but this could be added in the future._

**IMPORTANT:**  
**Expects Firebase Project to use Blaze plan, not Spark plan.  Blaze still provides generous free tier, but requires payment method to be set up.  Node 8 is being deprecated early 2021 and only Node 10+ will be supported, which is only supported on Blaze.**  

**If you wish to use the Spark plan then you must update the Node engine in package.json to '8' and re-run `npm install` before deploying for Firebase.**

## Getting Set Up ##

### Set up tools and authentication ##
Install Firebase CLI tools, if not installed already  

`npm install -g firebase-tools`  

Clone/fork repository and connect it to your Firebase project  

Download the `serviceAcountKey.json` from your Firebase project console and place in the `secret/` directory.

Create a token to use for `/messages` API authentication and save it in `secret/messageTokenKey.json`.  Only requests carrying this Bearer Token in the Authorization header can access the API, so also provide the token to whatever device/server is using the API.

```json
{
  "token": "my-bearer-token"
}
```
_Note: The `secret` directory is not tracked in git by default. Update the .gitignore if you want to track it._  

Update/uncomment `serviceAccount`, `databaseURL`, and `messageTokenKey` definitions in `config.js`.  

### Connect to your project ###
Below are two options for connecting to your existing Firebase project.  

First, make sure your Firebase CLI is connected to your account:  

`firebase login`


**Option 1 (Easiest)**  
If your Firebase project is pretty much blank and/or **you are ok with writing over any existing Cloud Functions, Firestore security rules, or indexes**, then you can just set the project as active and deploy.  
`firebase projects:list`  
`firebase use <project-id>`  
`firebase deploy`  

**Option 2 (Longer, Still Easy)**  
If you have any Firestore rules or indexes that you'd like to preserve, then you can go through the _slightly_ longer process of initializing the firebase project and manually choosing which items to overwrite from your existing project.  

`firebase init`  

*  Select `Functions` and `Firestore` as features
*  Select your project
*  Use the default `firestore.rules` file name
*  Select `y` or `N` to overwrite local rules from this repo with rules from your Firebase project
*  Repeat for indexes
*  Set language as `JavaScript`
*  Want to use ESLint? `Yes`
*  Answer prompts about overwriting `functions` files
    *  `package.json` ? If yes, make sure you add `cors` and `express`.
    *  `.eslintrc.json` ? Do what makes you happy.
    *  `index.js` ? If yes, be sure to export the `messages.js` module.
    *  `.gitignore` ? If yes, add `**/secret/**.json` if desired
*  Install dependencies? Probably `Yes`, unless you want to change the Node version (this repo uses Node 10)

`firebase deploy`


## Using the API ##
**This configuration assumes that client devices write their own device IDs to the Cloud Firestore in the same project under the `device_ids` collection with documents keyed by `user_id`.**  

You will receive your final cloud function URL upon deployment, but it should look like this:  

`https://<project_location>-<project_id>.cloudfunctions.net/messages`

### POST /subscribe & /unsubscribe ###
These endpoints will register and unregister a user's devices to receive messages for a given topic.  They both expect a POST body in the following shape:
```json
{
    "user": "some_user_id",
    "topic": "myTopic"
}
```

### POST /send ###
This endpoint will pass message data on to Firebase Cloud Messaging for publishing.  This can either be a `notification` or `data` message and can be sent to either a topic for subscribed users or directly to a list of users.

A `notification` message only contains text
```json
{
  "topic": "myTopic",
  "notification": {
    "title": "Short and Sweet!",
    "body": "Here's a little more detail.  Click to learn more!"
  }
}
```
A `data` message can contain any data you want, but must be handled more robustly on the client to properly display the notification.
```json
{
  "topic": "myTopic",
  "data": {
    "firstThing": 1,
    "otherKey": "otherValue",
    "moreKeys": ["more", "values"],
    "myMessage": "Show this text to the user",
    "imageURL": "http://maybe.you.want.a.pretty.picture.too"
  }
}
```

A message directly targeting a list of users can be sent in a similar fashion, but instead of providing a `topic` you would provided a `users` array.  The endpoint will fetch the appropraite device IDs for the list of users and send the message directly.  
```json
{
    "users": ["user1_id", "user2_id", "user3_id", "..."],
    "[data/notification]: { ... }
}
```

## Develop and Test Functions ##
Use the [Firebase Emulators Suite](https://firebase.google.com/docs/) to provide a sandbox on your local machine for testing without impacting production.  

`firebase serve`  
`firebase serve --only functions`

Modify code in `functions` directory (supports hot reload) and test against the provided `localhost:5000` host for the sandbox.  

When you are satisified, you can deploy the new changes to your Firebase Project  

`firebase deploy`

## Firestore Security Rules ##
This configuration also contains basic security rules in `firestore.rules` that govern access to the Cloud Firestore database in your project

Since we assume that device IDs are stored in `user_id` documents under a `device_ids` collection, the rule simply enforces that only the owning user can read or write their device IDs.
```
// Allow the user to write their own device IDs
match /device_ids/{userID} {
  allow read, write: if request.auth.uid == userID;
}
```
_The Admin SDK bypasses security rules, so rules only apply when using the Firebase Client SDK._


## Who do I talk to? ##

For questions or concerns, please contact jake@thesheets.me